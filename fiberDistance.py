# Script for determining the mean fiber distance in ceramic matrix composities

# Import dependencies
import glob
import numpy
import cv2
import matplotlib.pyplot
import csv

# Event handler for mouse events in figure
def onclick(event):
	global coordsLocal
	global ax
	# Select on left click
	if event.button == 1:
		ax.plot(event.xdata, event.ydata, color="blue", marker="o")
		coordsLocal.append((event.xdata, event.ydata))
	# Delete last selection on right click
	else:
		ax.lines = ax.lines[:-1]
		coordsLocal = coordsLocal[:-1]

# Get working directory
print("Enter path for working directory:")
path = input()

# Search for image files
files = glob.glob(path+"\*.jpg")

# Import images
imagesColor = [cv2.imread(file) for file in files]

# Grayscale
images = [cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) for image in imagesColor]

# Get image dimensions
dimensions = images[0].shape

coords = []
# Iterate over images
for image in imagesColor:
	# Plot image
	matplotlib.pyplot.ion()
	fig, ax = matplotlib.pyplot.subplots(1,1)
	ax.imshow(imagesColor[0])
	
	# Init variable
	coordsLocal = []

	# Connect event handler
	cid = fig.canvas.mpl_connect('button_press_event', onclick)
	
	# Wait for user to finish
	print("Use left mouse click to select points and right click to remove last point. Press Enter to finish.")
	input()
	
	#Disconnect event handler and close figure
	fig.canvas.mpl_disconnect(cid)
	matplotlib.pyplot.close(fig)
	
	# Save selected coordinates
	coords.append(coordsLocal)

print("Calculating")
	
# Calculate distances
distances = []
for coordsLocal in coords:
	coordsLocal = numpy.array(coordsLocal)
	distancesLocal = []
	for temp in coordsLocal:
		# Calculate distances
		temp = numpy.sum((coordsLocal - temp)**2,axis=1)**0.5
		# Select minimum distance
		temp = numpy.amin(temp[numpy.where(temp > 0)[0]])
		# Save
		distancesLocal.append(temp)
	distances.append(distancesLocal)

# Get mean and standard deviation
distancesMean = [numpy.mean(distancesLocal) for distancesLocal in distances]
distancesStd = [numpy.std(distancesLocal) for distancesLocal in distances]

# Get legend
print("Enter legend distance in pixels:")
legendPixels = int(input())
print("Enter legend distance in target unit:")
legendTarget = int(input())

# Convert distances to target unit
distancesMean = [i / legendPixels * legendTarget for i in distancesMean]
distancesStd = [i / legendPixels * legendTarget for i in distancesStd]

# Write output
with open(path + "\output.csv", mode='w', newline='') as csvFile:
    csvWriter = csv.writer(csvFile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    # Header
    csvWriter.writerow(["File", "Fibers", "Distance (mean)", "Distance (standard deviation)"])
    # Content
    for i in range(len(images)): 
        csvWriter.writerow([files[i].split("\\")[-1], len(coords[i]), distancesMean[i], distancesStd[i]])