# CMC Microstructural Analysis

Python scripts for easier microstructural analysis of ceramic matrix composites

## Getting started

1. Clone this repository to your desired location.
2. Install all required packages.
    ```
    pip install -r requirements.txt
    ```
3. Run the scripts.
    ```
    python fiberDistance.py
    python siliconArea.py
    ```

## fiberDistance.py

fiberDistance.py is a small tool to calculate the mean distance, its standard deviation and the total number of all fibers in a cross section image of a ceramic matrix composite sample.

Requirements:
* All images are in JPG format.
* All images share the same scale (pixel to distance ratio).

After running the script enter the path of the working directory where all cross section images can be found. One after the other each image is shown so you can select the fibers by clicking on their positions. If you selected a wrong position, click left anywhere in the image to unselect the latest position. After you have finished selecting all fibers in the image press Enter in the command window to proceed with the next image. When all images in the working directory are marked the tool will ask for the scale of the images. Enter the length of your reference segment in pixels and in your desired target unit. The tool will then calculate mean distance, its standard deviation and the total number of all fibers for all images and save the results in a file "output.csv" in the specified working directory.

## siliconArea.py

siliconArea.py is a small tool to calculate the area percentage of silicon in a cross-section image of a ceramic matrix composite sample.

Requirements:
* All images are in JPG format.
* Silicon appears white in the image, all other parts of the sample arbitrarily colored and the surrounding area of the sample black.

After running the script enter the path of the working directory where all cross section images can be found. The tool will calculate the area percentage of silicon in all avaiable images and save the results in a file "output.csv" in the specified working directory.
