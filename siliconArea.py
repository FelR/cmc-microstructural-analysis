# Script for determining the area percentage of silicon in in ceramic matrix composities 

# Import dependencies
import glob
import numpy
import cv2
import csv

# Get working directory
print("Enter path for working directory:")
path = input()

# Search for image files
files = glob.glob(path+"\*.jpg")

# Import images
imagesColor = [cv2.imread(file) for file in files]

# Grayscale
images = [cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) for image in imagesColor]

# Get complete area
area = [image.shape[0]*image.shape[1] for image in images]

# Get black area
areaBlack = [len(numpy.column_stack(numpy.where(image < 3))) for image in images]

# Get white area
areaWhite = [len(numpy.column_stack(numpy.where(image > 253))) for image in images]

# Get share of white area
areaWhiteShare = [areaWhite[i]/(area[i]-areaBlack[i]) for i in range(len(images))]

# Write output
with open(path + "\output.csv", mode='w', newline='') as csvFile:
    csvWriter = csv.writer(csvFile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    # Header
    csvWriter.writerow(["File", "Share of white area"])
    # Content
    for i in range(len(images)): 
        csvWriter.writerow([files[i].split("\\")[-1],areaWhiteShare[i]])
